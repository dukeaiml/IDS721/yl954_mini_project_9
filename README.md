# Yl954 Mini Project 9
This project deploys a Streamlit App with a Hugging Face Model.

## Author
Yuanzhi Lou

## Demo
[https://lyuanzhi-mini9.streamlit.app/](https://lyuanzhi-mini9.streamlit.app/)

## Dependencies
```
transformers
tensorflow
tf-keras
```

## Model
```openai-gpt```

## Implementation
```
import streamlit as st
from transformers import pipeline

model = pipeline("text-generation", model="openai-gpt")
input_text = st.text_area("Enter Text", height=100)
generate_button = st.button("Generate Text")
if generate_button:
    if input_text:
        output_text = model(input_text)[0]["generated_text"]
        st.markdown("<div>" + output_text + "</div>", unsafe_allow_html=True)
```

## Deploy
1. go to [https://share.streamlit.io/](https://share.streamlit.io/), sign up an account
2. sign in, and connect to github
3. create a repo in github to contain ```LLM.py``` and ```requirements.txt```
4. create a new app using existing repo
5. waiting for completion

## screenshot
![](web.png)